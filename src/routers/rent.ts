import express, { NextFunction, Request, Response, Router } from 'express';
import { RentCarController } from '../controllers/RentCarController';

const router: Router = express.Router();
const controller: RentCarController = new RentCarController();

router.post('/', async (request: Request, response: Response, next: NextFunction) => {
  return await controller.create(request, response, next);
});

router.put('rent-finalizer', async (request: Request, response: Response, next: NextFunction) => {
  return await controller.finalizerRent(request, response, next);
});

export const rentRouter: Router = router;
