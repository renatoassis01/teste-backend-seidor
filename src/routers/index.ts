import express, { Router } from 'express';
import { carsRouter } from './car';
import { rentRouter } from './rent';

const router: Router = express.Router();
router.use('/cars', carsRouter);
router.use('/rent', rentRouter);

export default router;
