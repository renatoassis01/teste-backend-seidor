import express, { NextFunction, Request, Response, Router } from 'express';
import { CarController } from '../controllers/CarController';

const router: Router = express.Router();
const controller: CarController = new CarController();

router.get('/search', async (request: Request, response: Response, next: NextFunction) => {
  return await controller.findMany(request, response, next);
});

router.get('/search/:carId', async (request: Request, response: Response, next: NextFunction) => {
  return await controller.findByCode(request, response, next);
});

router.post('/', async (request: Request, response: Response, next: NextFunction) => {
  return await controller.create(request, response, next);
});

export const carsRouter: Router = router;
