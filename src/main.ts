import * as dotenv from 'dotenv';
import express from 'express';
import 'reflect-metadata';
import Application from './application';
dotenv.config();

const application: Application = new Application(express());
application.start();
