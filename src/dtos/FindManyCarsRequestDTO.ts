import { IsOptional } from 'class-validator';

/**
 * classe poco para validação de um requisição
 */
export class FindManyCarsRequestDTO {
  @IsOptional()
  color?: string;

  @IsOptional()
  model?: string;
}
