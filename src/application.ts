import bodyParser from 'body-parser';
import chalk from 'chalk';
import express, { Request, Response } from 'express';
import http from 'http';
import error from './commom/middlewares/ErrorMiddlware';
import request from './commom/middlewares/RequestsMiddlware';
import routers from './routers/index';
const host = process.env.APPLICATION_PORT || 4200;
const hostname = process.env.APPLICATION_HOSTNAME || 'localhost';
const envoirment = process.env.NODE_ENV || 'development';
const urlBase = `http://${hostname}:${host}`;
const banner = `
envoirment: ${chalk.yellow(envoirment)}
running at: ${chalk.green(urlBase)}
`;

export default class Application {
  public server: http.Server;
  constructor(public app: express.Application) {}

  public start(): void {
    this.setupMiddlware();
    this.setup();
    this.setupHelth();
  }

  private setup(): void {
    this.server = this.app.listen(host, () => console.info(banner));
  }

  private setupMiddlware(): void {
    this.app.use(error);
    this.app.use(request);
    this.app.use(bodyParser.urlencoded());
    this.app.use(bodyParser.json());
    this.app.use(routers);
  }

  private setupHelth(): void {
    this.app.get('/health', (req: Request, res: Response) => res.json('works!'));
  }
}
