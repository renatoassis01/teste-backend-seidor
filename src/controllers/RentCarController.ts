import { NextFunction, Request, Response } from 'express';
import { RentCarManager } from '../services/RentCarManager';

export class RentCarController {
  private readonly rentCarManager: RentCarManager;

  constructor() {
    this.rentCarManager = new RentCarManager();
  }

  /***
   * Criar um novo alguel
   */
  public async create(request: Request, response: Response, next: NextFunction): Promise<any> {
    const rent = await this.rentCarManager.create(request.body);
    if (!rent) return response.status(406).send('Carro não disponível');
    return response.status(200).send(rent);
  }

  /**
   * concluí o alguel de um carro
   */
  public async finalizerRent(request: Request, response: Response, next: NextFunction): Promise<any> {
    await this.rentCarManager.finalizerRent(+request.params['carId'], request.body);
    return response.status(200);
  }
}
