import { NextFunction, Request, Response } from 'express';
import { makeValidateBody } from 'express-class-validator';
import { FindManyCarsRequestDTO } from '../dtos/FindManyCarsRequestDTO';
import { CarManager } from '../services/CarManager';

export class CarController {
  private readonly carManager: CarManager;

  constructor() {
    this.carManager = new CarManager();
  }

  public async findMany(request: Request, response: Response, next: NextFunction): Promise<any> {
    makeValidateBody(FindManyCarsRequestDTO);
    const cars = await this.carManager.findMany(request.body);
    return response.status(200).send(cars);
  }

  public async findByCode(request: Request, response: Response, next: NextFunction): Promise<any> {
    const cars = await this.carManager.findByCode(+request.params['carId']);
    if (!cars) return response.status(404).send('car not found');
    return response.status(200).send(cars);
  }

  public async create(request: Request, response: Response, next: NextFunction): Promise<any> {
    const cars = await this.carManager.create(request.body);
    return response.status(200).send(cars);
  }

  public async update(request: Request, response: Response, next: NextFunction): Promise<any> {
    const cars = await this.carManager.update(+request.params['carId'], request.body);
    if (!cars) return response.status(404).send('car not found');
    return response.status(200).send(cars);
  }
}
