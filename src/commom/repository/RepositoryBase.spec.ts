import {
  Column,
  Connection,
  createConnection,
  CreateDateColumn,
  Entity,
  getConnection,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { BaseModel } from '../generics/models/BaseModel';
import { BaseRepository } from './RepositoryBase';

/**
 * Entidade para testar o repositório base
 */
@Entity()
class TodoModel implements BaseModel {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  name: string;

  @Column({ default: false })
  done?: boolean;

  @Column({ nullable: true })
  concluedAt?: Date;

  @CreateDateColumn()
  createAt?: Date;

  @UpdateDateColumn()
  updateAt?: Date;
}

/**
 * Repositório para testes do BaseRepository.
 */
class TodoRepository extends BaseRepository<TodoModel> {
  constructor(private readonly _connection: Connection = getConnection()) {
    super(_connection.getRepository(TodoModel));
  }
}

let todoRepository: TodoRepository;
let todo: TodoModel | undefined;

describe('Suite de testes de BaseRepository', () => {
  beforeAll(async () => {
    const conn = await createConnection({
      type: 'sqlite',
      database: ':memory:',
      dropSchema: true,
      entities: [TodoModel],
      synchronize: true,
      logging: false,
    });
    todoRepository = new TodoRepository();
    return conn;
  });

  afterAll(() => {
    let conn = getConnection();
    return conn.close();
  });

  test('deve salvar uma entidade', async () => {
    todo = await todoRepository.save({
      name: 'todo1',
    });
    expect(todo.name).toBe('todo1');
  });

  test('deve buscar uma entidade pelo código', async () => {
    todo = await todoRepository.findByCode(1);
    expect(todo?.name).toBe('todo1');
  });

  test('deve buscar todas as entidades no banco', async () => {
    const todos = await todoRepository.findAll();
    expect(todos).not.toEqual(undefined);
  });

  test('deve buscar todas as entidades no banco por uma propriedade', async () => {
    const todos = await todoRepository.findManyByProperties({ name: 'todo1' });
    expect(todos).not.toEqual(undefined);
  });

  test('deve atualizar uma entidade', async () => {
    await todoRepository.partialUpdate(1, { name: 'todo2' });
    todo = await todoRepository.findByCode(1);
    expect(todo?.name).toBe('todo2');
  });

  test('deve buscar uma entidade por uma proriedade', async () => {
    todo = await todoRepository.findOneByProperties({ name: 'todo2' });
    expect(todo?.name).toBe('todo2');
  });

  test('deve deletar uma entidade', async () => {
    await todoRepository.delete(todo!);
    todo = await todoRepository.findByCode(1);
    expect(todo).toEqual(undefined);
  });
});
