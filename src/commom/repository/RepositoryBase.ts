import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { IBaseRepository } from '../generics/interfaces/IBaseRepository';
import { BaseModel } from '../generics/models/BaseModel';

/**
 * Repositório base com implementação de alguns métodos(CRUD)
 */
export abstract class BaseRepository<T extends BaseModel> implements IBaseRepository<T> {
  /**
   * contrutors
   * @param _repository
   */
  constructor(protected readonly _repository: Repository<T>) {}

  /**
   * Salva uma entidade
   * @param object
   */
  public async save(object: T): Promise<T> {
    return await this._repository.save<any>(object, { reload: true });
  }

  /**
   * Retorna todas a entidades
   */
  public async findAll(): Promise<T[] | undefined> {
    return await this._repository.find();
  }

  /**
   * Busca varias entidades de acordo com filtro
   * @param properties
   * @param relations
   */
  public async findManyByProperties(properties: QueryDeepPartialEntity<T>, relations?: string[]): Promise<T[] | undefined> {
    const options: FindManyOptions = { where: properties, relations };
    return await this._repository.find(options);
  }

  /**
   * Deleta a entidade do banco
   * @param object
   */
  public async delete(object: T): Promise<void> {
    await this._repository.remove(object);
  }

  /**
   * Filtra uma entidade por seu id
   * @param id
   * @param relations
   */
  public async findByCode(id: number, relations?: string[]): Promise<T | undefined> {
    const options: FindOneOptions = { relations };
    return await this._repository.findOne(id, options);
  }

  /**
   * Busca por uma entidade por qualquer propriedade
   * @param idOrProperties
   * @param relations
   */
  public async findOneByProperties(idOrProperties: QueryDeepPartialEntity<T>, relations?: string[]): Promise<T | undefined> {
    const filters: any = typeof idOrProperties === 'object' ? idOrProperties : { code: idOrProperties };
    const options: FindOneOptions = { relations };
    return await this._repository.findOne(filters, options);
  }

  /**
   * Atualiza um objeto parcialmente
   * @param id
   * @param partialEntity
   */
  public async partialUpdate(id: number, partialEntity: QueryDeepPartialEntity<T>): Promise<void> {
    await this._repository.update(id, partialEntity);
  }
}
