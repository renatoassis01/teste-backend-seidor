/**
 * classe util para verificação de ambiente
 */
export class EnvironmentUtils {
  public static isDevEnvironment(): boolean {
    return process.env.NODE_ENV === 'development';
  }
}
