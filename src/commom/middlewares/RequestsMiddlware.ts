import { NextFunction, Request, Response } from 'express';
import { logger } from '../logger/logger';

/**
 * Middlware para tratamento de requisições
 */
export default function (req: Request, res: Response, next: NextFunction): void {
  const msg = `${req.method} - ${req.originalUrl}`;
  logger.info(msg);
  next();
}
