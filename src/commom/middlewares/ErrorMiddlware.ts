import { NextFunction, Request, Response } from 'express';
import { logger } from '../logger/logger';

/**
 * Middlware para tratamento de erros
 */
export default function (err: any, req: Request, res: Response, next: NextFunction): void {
  // set locals, only providing error in development
  res.locals.message = err.message;
  logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  // render the error page
  res.status(err.status || 500);
  res.render('error');
}
