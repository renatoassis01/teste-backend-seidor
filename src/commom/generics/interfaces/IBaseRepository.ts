import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { BaseModel } from '../models/BaseModel';

/**
 * Interface base para os repositorios
 */
export interface IBaseRepository<T extends BaseModel> {
  save(object: T): Promise<T>;

  findAll(): Promise<T[] | undefined>;

  findManyByProperties(properties: QueryDeepPartialEntity<T>, relations?: string[]): Promise<T[] | undefined>;

  delete(object: T): Promise<void>;

  findByCode(id: number, relations?: string[]): Promise<T | undefined>;

  findOneByProperties(idOrProperties: number | QueryDeepPartialEntity<T>, relations?: string[]): Promise<T | undefined>;

  partialUpdate(id: number, partialEntity: QueryDeepPartialEntity<T>): Promise<void>;
}
