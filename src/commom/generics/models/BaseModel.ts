export abstract class BaseModel {
  id?: number;
  createAt?: Date;
  updateAt?: Date;
}
