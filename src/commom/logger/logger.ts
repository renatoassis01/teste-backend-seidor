import chalk from 'chalk';
import { Console } from 'console';

enum levels {
  TRACE = 'TRACE',
  DEBUG = 'DEBUG',
  INFO = 'INFO',
  WARN = 'WARN',
  ERROR = 'ERROR',
  FATAL = 'FATAL',
}

/*
  trace = 'white',
  debug = 'green',
  info = 'green',
  warn = 'yellow',
  error = 'red',
  fatal = 'red',
*/

class Logger {
  private logger: Console;

  constructor() {
    this.logger = new Console(process.stdout);
  }

  private format(level: levels, color: chalk.Chalk, message: any): string {
    const timestamp = new Date().toDateString();
    return `${timestamp} [${color(level)}]: ${message}`;
  }

  trace(msg: any) {
    const color = chalk.white;
    this.logger.log(this.format(levels.TRACE, color, msg));
  }

  debug(msg: any) {
    const color = chalk.green;
    this.logger.debug(this.format(levels.DEBUG, color, msg));
  }

  info(msg: any) {
    const color = chalk.green;
    this.logger.info(this.format(levels.INFO, color, msg));
  }

  warn(msg: any) {
    const color = chalk.yellow;
    this.logger.warn(this.format(levels.WARN, color, msg));
  }

  error(msg: any) {
    const color = chalk.red;
    this.logger.error(this.format(levels.ERROR, color, msg));
  }

  fatal(msg: any) {
    const color = chalk.red;
    this.logger.error(this.format(levels.FATAL, color, msg));
  }
}

export const logger = new Logger();
