import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { BaseModel } from '../commom/generics/models/BaseModel';
import { CarModel } from './CarModel';
import { DriverModel } from './DriverModel';

@Entity('c10_rentcar')
export class RentCarModel implements BaseModel {
  @PrimaryGeneratedColumn({ name: 'rca_id' })
  id?: number;

  @Column({ name: 'rca_reason' })
  reason: string;

  @Column({ name: 'rca_stardate', type: 'date' })
  startDate: Date;

  @Column({ name: 'rca_enddate', type: 'date', nullable: true })
  endDate?: Date;

  @Column({ name: 'rca_drvid' })
  driverId: number;

  @Column({ name: 'rca_carid' })
  carId: number;

  @CreateDateColumn({ name: 'rca_createat' })
  createAt?: Date;

  @UpdateDateColumn({ name: 'rca_updateat' })
  updateAt?: Date;

  @OneToOne(() => CarModel)
  @JoinColumn({ name: 'rca_carid' })
  car?: CarModel;

  @OneToOne(() => DriverModel)
  @JoinColumn({ name: 'rca_drvid' })
  driver?: DriverModel;
}
