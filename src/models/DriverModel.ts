import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { BaseModel } from '../commom/generics/models/BaseModel';
import { RentCarModel } from './RentCarModel';

@Entity('c10_driver')
export class DriverModel implements BaseModel {
  @PrimaryGeneratedColumn({ name: 'drv_id' })
  id?: number;

  @Column({ name: 'drv_name' })
  name: string;

  @CreateDateColumn({ name: 'drv_createat' })
  createAt?: Date;

  @UpdateDateColumn({ name: 'drv_updateat' })
  updateAt?: Date;

  @OneToMany(() => RentCarModel, (rca) => rca.driver, { cascade: true })
  rent?: RentCarModel[];
}
