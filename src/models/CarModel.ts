import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { BaseModel } from '../commom/generics/models/BaseModel';
import { RentCarModel } from './RentCarModel';

@Entity('c10_car')
export class CarModel implements BaseModel {
  @PrimaryGeneratedColumn({ name: 'car_id' })
  id?: number;

  @Column({ name: 'car_licenseplate' })
  licensePlate: string;

  @Column({ name: 'car_model' })
  model: string;

  @Column({ name: 'car_color' })
  color: string;

  @Column({ name: 'car_year' })
  year: string;

  @CreateDateColumn({ name: 'car_createat' })
  createAt?: Date;

  @UpdateDateColumn({ name: 'car_updateat' })
  updateAt?: Date;

  @OneToMany(() => RentCarModel, (rca) => rca.car, { cascade: true })
  rent?: RentCarModel[];
}
