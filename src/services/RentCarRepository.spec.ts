import { createConnection } from 'typeorm';
import { CarModel } from '../models/CarModel';
import { DriverModel } from '../models/DriverModel';
import { RentCarModel } from '../models/RentCarModel';
import { CarManager } from './CarManager';
import { DriverManager } from './DriverManager';
import { RentCarRepository } from './RentCarRepository';

let driverManager: DriverManager;
let carManager: CarManager;
let rentCarRepository: RentCarRepository;
let newDriver: DriverModel;
let newCar: CarModel;

let newDriver2: DriverModel;
let newCar2: CarModel;

const driver = {
  name: 'Carlos Antônio',
};

const driver2 = {
  name: 'Toretto',
};

const car = {
  licensePlate: 'xxxxx-zzzzz',
  model: 'C10',
  color: 'laranja',
  year: '1979',
};

const car2 = {
  licensePlate: 'wwwww-dddd',
  model: 'Uno',
  color: 'branco',
  year: '2020',
};

describe('Suite de testes de RentCarRepository', () => {
  beforeAll(async () => {
    const conn = await createConnection({
      type: 'sqlite',
      database: ':memory:',
      dropSchema: true,
      entities: [CarModel, DriverModel, RentCarModel],
      synchronize: true,
      logging: false,
    });

    driverManager = new DriverManager();
    carManager = new CarManager();
    rentCarRepository = new RentCarRepository();

    newDriver = await driverManager.create(driver);
    newCar = await carManager.create(car);
    newDriver2 = await driverManager.create(driver2);
    newCar2 = await carManager.create(car2);

    // adicionando aluguel 1
    await rentCarRepository.save({
      carId: newCar.id,
      driverId: newDriver.id,
      reason: 'rolezinho',
      startDate: new Date(),
    });

    // adicionando aluguel 2
    await rentCarRepository.save({
      carId: newCar2.id,
      driverId: newDriver2.id,
      reason: 'drift em tokyo',
      startDate: new Date(),
      endDate: new Date(),
    });

    return conn;
  });

  test('deve retornar false para carro não disponível', async () => {
    const result = await rentCarRepository.isAvailable(1);
    expect(result).toEqual(false);
  });

  test('deve retornar verdadeiro para carro disponível', async () => {
    const result = await rentCarRepository.isAvailable(2);
    expect(result).toEqual(true);
  });
});
