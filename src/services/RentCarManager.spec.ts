import { createConnection } from 'typeorm';
import { CarModel } from '../models/CarModel';
import { DriverModel } from '../models/DriverModel';
import { RentCarModel } from '../models/RentCarModel';
import { CarManager } from './CarManager';
import { DriverManager } from './DriverManager';
import { RentCarManager } from './RentCarManager';

let driverManager: DriverManager;
let carManager: CarManager;
let rentCarManager: RentCarManager;
let newDriver: DriverModel;
let newCar: CarModel;
let newRent: RentCarModel;

const driver = {
  name: 'Carlos Antônio',
};

const car = {
  licensePlate: 'wwwww-dddd',
  model: 'Uno',
  color: 'branco',
  year: '2020',
};

describe('Suite de testes de RentCarRepository', () => {
  beforeAll(async () => {
    const conn = await createConnection({
      type: 'sqlite',
      database: ':memory:',
      dropSchema: true,
      entities: [CarModel, DriverModel, RentCarModel],
      synchronize: true,
      logging: true,
    });

    driverManager = new DriverManager();
    carManager = new CarManager();
    rentCarManager = new RentCarManager();

    newDriver = await driverManager.create(driver);
    newCar = await carManager.create(car);
    return conn;
  });

  test('deve retornar uma instância de RentCarModel', async () => {
    newRent = await rentCarManager.create({
      carId: newCar.id,
      driverId: newDriver.id,
      reason: 'rolezinho',
      startDate: new Date(),
    });
    expect(newRent.reason).toEqual('rolezinho');
  });

  test('deve retornar uma instância de RentCarModel com endDate diferente de null', async () => {
    await rentCarManager.finalizerRent(newRent.id, new Date());
    newRent = await rentCarManager.findByCode(newRent.id);
    expect(newRent.endDate).not.toBeNull();
  });
});
