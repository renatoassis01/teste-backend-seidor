import { Connection, createConnection, getConnection } from 'typeorm';
import { BaseRepository } from '../commom/repository/RepositoryBase';
import connectionOptions from '../config/database';
import { DriverModel } from '../models/DriverModel';
/**
 * Repositório de motoristas
 */
createConnection(connectionOptions);
export class DriverRepository extends BaseRepository<DriverModel> {
  constructor(private readonly _connection: Connection = getConnection()) {
    super(_connection.getRepository(DriverModel));
  }
}
