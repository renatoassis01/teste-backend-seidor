import { CarModel } from '../models/CarModel';
import { CarRepository } from './CarRepository';

/**
 * Classe para gerenciar os dados de carros
 */
export class CarManager {
  constructor(private readonly _carRepository: CarRepository = new CarRepository()) {}

  /**
   * lista carros
   * @param filters
   */
  public async findMany(filters: any): Promise<CarModel[] | undefined> {
    return this._carRepository.findManyByProperties({ ...filters });
  }

  /**
   * filtra um carros por código
   * @param id
   */
  public async findByCode(id: number): Promise<CarModel | undefined> {
    return await this._carRepository.findByCode(id);
  }

  /**
   * Cria um registro de carro
   * @param newCar
   */
  public async create(newCar: any): Promise<CarModel> {
    return await this._carRepository.save(newCar);
  }

  /**
   * Atualiza um registro de um carro
   * @param id
   * @param updateCar
   */
  public async update(id: number, updateCar: any): Promise<CarModel | undefined> {
    const car = await this._carRepository.findByCode(id);
    if (!car) return undefined;
    await this._carRepository.partialUpdate(id, { ...updateCar });
    return await this._carRepository.findByCode(id);
  }
}
