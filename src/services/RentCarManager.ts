import { RentCarModel } from '../models/RentCarModel';
import { RentCarRepository } from './RentCarRepository';

/**
 * Classe para gerenciar os dados de alguel de carros
 */
export class RentCarManager {
  constructor(private readonly _rentCarRepository: RentCarRepository = new RentCarRepository()) {}

  /**
   * Busca um aluguel por código
   * @param rentId
   */
  public async findByCode(rentId: number): Promise<RentCarModel> {
    return await this._rentCarRepository.findByCode(rentId);
  }

  /**
   * Cria um aluguel de um carro
   * @param newRent
   */
  public async create(newRent: any): Promise<RentCarModel | undefined> {
    const isAvailable = await this._rentCarRepository.isAvailable(newRent.carId);
    if (isAvailable) return await this._rentCarRepository.save(newRent);
    return undefined;
  }

  /**
   * Finaliza um aluguel de um carro
   * @param rentId
   * @param endDate
   */
  public async finalizerRent(rentId: number, endDate: Date): Promise<void> {
    await this._rentCarRepository.partialUpdate(rentId, { endDate });
  }
}
