import { DriverModel } from '../models/DriverModel';
import { DriverRepository } from './DriverRepository';

/**
 * Classe para gerenciar os dados de motoristas
 */
export class DriverManager {
  constructor(private readonly _driverRepository: DriverRepository = new DriverRepository()) {}

  /**
   * lista carros
   * @param filters
   */
  public async findMany(filters: any): Promise<DriverModel[] | undefined> {
    return this._driverRepository.findManyByProperties({ ...filters });
  }

  /**
   * filtra um motorista por código
   * @param id
   */
  public async findByCode(id: number): Promise<DriverModel | undefined> {
    return await this._driverRepository.findByCode(id);
  }

  /**
   * Cria um registro de motorista
   * @param newDriver
   */
  public async create(newDriver: any): Promise<DriverModel> {
    return await this._driverRepository.save(newDriver);
  }

  /**
   * Atualiza um registro de um motorista
   * @param id
   * @param updateDriver
   */
  public async update(id: number, updateDriver: any): Promise<DriverModel | undefined> {
    const driver = await this._driverRepository.findByCode(id);
    if (!driver) return undefined;
    await this._driverRepository.partialUpdate(id, { ...updateDriver });
    return await this._driverRepository.findByCode(id);
  }
}
