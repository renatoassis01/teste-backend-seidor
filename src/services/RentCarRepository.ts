import { Connection, createConnection, getConnection, IsNull } from 'typeorm';
import { BaseRepository } from '../commom/repository/RepositoryBase';
import connectionOptions from '../config/database';
import { RentCarModel } from '../models/RentCarModel';
/**
 * Repositório de motoristas
 */
createConnection(connectionOptions);
export class RentCarRepository extends BaseRepository<RentCarModel> {
  constructor(private readonly _connection: Connection = getConnection()) {
    super(_connection.getRepository(RentCarModel));
  }

  /**
   * verifica a disponibilidade de um carro para aluguel
   * @param carId
   */
  public async isAvailable(carId: number): Promise<boolean> {
    const result = await this._repository.count({ carId, endDate: IsNull() });
    return result === 0;
  }
}
