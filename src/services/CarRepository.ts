import { Connection, createConnection, getConnection } from 'typeorm';
import { BaseRepository } from '../commom/repository/RepositoryBase';
import connectionOptions from '../config/database';
import { CarModel } from '../models/CarModel';
/**
 * Repositório de carros
 */

createConnection(connectionOptions);
export class CarRepository extends BaseRepository<CarModel> {
  constructor(private readonly _connection: Connection = getConnection()) {
    super(_connection.getRepository(CarModel));
  }
}
