import path from 'path';
import { ConnectionOptions } from 'typeorm';

const connectionOptions: ConnectionOptions = {
  type: 'sqlite',
  database: 'database.sqlite',
  synchronize: true,
  logging: true,
  entities: [path.join(__dirname, '../models/**{.ts,.js}')],
};

export default connectionOptions;
