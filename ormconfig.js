const path = require('path');
const options = {
  type: 'sqlite',
  database: 'database.sqlite',
  synchronize: true,
  logging: true,
  entities: [path.join(__dirname, 'src/models/*.ts')],
  migrations: ['migrations/*.ts'],
  cli: {
    migrationsDir: 'migrations',
  },
};

module.exports = options;
