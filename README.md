
# requisitos necessários
  
  1. nodejs >= v12.16.2
# Tecnologias utilizadas

 1. typescript
 2. express
 3. typeorm
 4. sqllite
 
 # rodar o projeto

 ```bash
 npm install
 npm run test
 ```

 ```
 npm install -g ts-node
 ```

```
 cd c10-backend
```

 ```bash
  ts-node ./node_modules/typeorm/cli.js migration:run
 ```

 ```bash
  npm run start:dev
 ```

