import { MigrationInterface, QueryRunner } from 'typeorm';

export class initial1607952303411 implements MigrationInterface {
  name = 'initial1607952303411';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "c10_driver" ("drv_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "drv_name" varchar NOT NULL, "drv_createat" datetime NOT NULL DEFAULT (datetime('now')), "drv_updateat" datetime NOT NULL DEFAULT (datetime('now')))`,
    );
    await queryRunner.query(
      `CREATE TABLE "c10_rentcar" ("rca_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "rca_reason" varchar NOT NULL, "rca_stardate" date NOT NULL, "rca_enddate" date, "rca_drvid" integer NOT NULL, "rca_carid" integer NOT NULL, "rca_createat" datetime NOT NULL DEFAULT (datetime('now')), "rca_updateat" datetime NOT NULL DEFAULT (datetime('now')), CONSTRAINT "REL_2560c35644fd2505bde4ce8440" UNIQUE ("rca_carid"), CONSTRAINT "REL_a2f009c59febc83a8b7a93bfa4" UNIQUE ("rca_drvid"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "c10_car" ("car_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "car_licenseplate" varchar NOT NULL, "car_model" varchar NOT NULL, "car_color" varchar NOT NULL, "car_year" varchar NOT NULL, "car_createat" datetime NOT NULL DEFAULT (datetime('now')), "car_updateat" datetime NOT NULL DEFAULT (datetime('now')))`,
    );
    await queryRunner.query(
      `CREATE TABLE "temporary_c10_rentcar" ("rca_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "rca_reason" varchar NOT NULL, "rca_stardate" date NOT NULL, "rca_enddate" date, "rca_drvid" integer NOT NULL, "rca_carid" integer NOT NULL, "rca_createat" datetime NOT NULL DEFAULT (datetime('now')), "rca_updateat" datetime NOT NULL DEFAULT (datetime('now')), CONSTRAINT "REL_2560c35644fd2505bde4ce8440" UNIQUE ("rca_carid"), CONSTRAINT "REL_a2f009c59febc83a8b7a93bfa4" UNIQUE ("rca_drvid"), CONSTRAINT "FK_2560c35644fd2505bde4ce8440a" FOREIGN KEY ("rca_carid") REFERENCES "c10_car" ("car_id") ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT "FK_a2f009c59febc83a8b7a93bfa45" FOREIGN KEY ("rca_drvid") REFERENCES "c10_driver" ("drv_id") ON DELETE NO ACTION ON UPDATE NO ACTION)`,
    );
    await queryRunner.query(
      `INSERT INTO "temporary_c10_rentcar"("rca_id", "rca_reason", "rca_stardate", "rca_enddate", "rca_drvid", "rca_carid", "rca_createat", "rca_updateat") SELECT "rca_id", "rca_reason", "rca_stardate", "rca_enddate", "rca_drvid", "rca_carid", "rca_createat", "rca_updateat" FROM "c10_rentcar"`,
    );
    await queryRunner.query(`DROP TABLE "c10_rentcar"`);
    await queryRunner.query(`ALTER TABLE "temporary_c10_rentcar" RENAME TO "c10_rentcar"`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "c10_rentcar" RENAME TO "temporary_c10_rentcar"`);
    await queryRunner.query(
      `CREATE TABLE "c10_rentcar" ("rca_id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "rca_reason" varchar NOT NULL, "rca_stardate" date NOT NULL, "rca_enddate" date, "rca_drvid" integer NOT NULL, "rca_carid" integer NOT NULL, "rca_createat" datetime NOT NULL DEFAULT (datetime('now')), "rca_updateat" datetime NOT NULL DEFAULT (datetime('now')), CONSTRAINT "REL_2560c35644fd2505bde4ce8440" UNIQUE ("rca_carid"), CONSTRAINT "REL_a2f009c59febc83a8b7a93bfa4" UNIQUE ("rca_drvid"))`,
    );
    await queryRunner.query(
      `INSERT INTO "c10_rentcar"("rca_id", "rca_reason", "rca_stardate", "rca_enddate", "rca_drvid", "rca_carid", "rca_createat", "rca_updateat") SELECT "rca_id", "rca_reason", "rca_stardate", "rca_enddate", "rca_drvid", "rca_carid", "rca_createat", "rca_updateat" FROM "temporary_c10_rentcar"`,
    );
    await queryRunner.query(`DROP TABLE "temporary_c10_rentcar"`);
    await queryRunner.query(`DROP TABLE "c10_car"`);
    await queryRunner.query(`DROP TABLE "c10_rentcar"`);
    await queryRunner.query(`DROP TABLE "c10_driver"`);
  }
}
