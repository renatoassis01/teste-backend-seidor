import { MigrationInterface, QueryRunner } from 'typeorm';

export class seeds1607953741495 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`INSERT INTO "c10_driver" ("drv_name") VALUES('Carlos Antônio');`);
    await queryRunner.query(`INSERT INTO "c10_driver" ("drv_name") VALUES('Macgayver Marques');`);
    await queryRunner.query(`INSERT INTO "c10_driver" ("drv_name") VALUES('Emerson Silva');`);
    await queryRunner.query(`INSERT INTO "c10_driver" ("drv_name") VALUES('Jairo Barbosa');`);
    await queryRunner.query(`INSERT INTO "c10_driver" ("drv_name") VALUES('João Otávio');`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
