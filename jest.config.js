module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testPathIgnorePatterns: ['dist/'],
  testMatch: ['**/src/**/*.spec.(ts)'],
  modulePaths: ['<rootDir>'],
  moduleNameMapper: {
    '~(.*)$': '<rootDir>/$1',
  },
};
